# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
PivotalClone::Application.config.secret_key_base = 'd9d4317d971039148ac3b9fcda1c42b512af8d98a3f5434736939c058ec8aafb0ce191375ba979755b74c64d90a18fb90dd6ce2df8be98d5d9cd8176cf993cfe'
