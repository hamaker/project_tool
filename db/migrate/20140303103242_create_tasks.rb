class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.references :story, index: true
      t.string :name
      t.boolean :status

      t.timestamps
    end
  end
end
