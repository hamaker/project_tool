class ChangeStoryStatusType < ActiveRecord::Migration
  def up
    change_column :stories, :status, :integer
  end
  def down
    change_column :stories, :status, :string
  end
end
