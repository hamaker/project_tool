class CreateStories < ActiveRecord::Migration
  def change
    create_table :stories do |t|
      t.references :owner, index: true
      t.integer :points
      t.string :status
      t.string :story_type
      t.references :requester, index: true
      t.text :description

      t.timestamps
    end
  end
end
