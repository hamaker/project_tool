require 'spec_helper'

describe "stories/new" do
  before(:each) do
    assign(:story, stub_model(Story,
      :owner => nil,
      :points => 1,
      :status => "MyString",
      :story_type => "MyString",
      :requester => nil,
      :description => "MyText"
    ).as_new_record)
  end

  it "renders new story form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", stories_path, "post" do
      assert_select "input#story_owner[name=?]", "story[owner]"
      assert_select "input#story_points[name=?]", "story[points]"
      assert_select "input#story_status[name=?]", "story[status]"
      assert_select "input#story_story_type[name=?]", "story[story_type]"
      assert_select "input#story_requester[name=?]", "story[requester]"
      assert_select "textarea#story_description[name=?]", "story[description]"
    end
  end
end
