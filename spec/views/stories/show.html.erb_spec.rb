require 'spec_helper'

describe "stories/show" do
  before(:each) do
    @story = assign(:story, stub_model(Story,
      :owner => nil,
      :points => 1,
      :status => "Status",
      :story_type => "Story Type",
      :requester => nil,
      :description => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/1/)
    rendered.should match(/Status/)
    rendered.should match(/Story Type/)
    rendered.should match(//)
    rendered.should match(/MyText/)
  end
end
