require 'spec_helper'

describe "stories/index" do
  before(:each) do
    assign(:stories, [
      stub_model(Story,
        :owner => nil,
        :points => 1,
        :status => "Status",
        :story_type => "Story Type",
        :requester => nil,
        :description => "MyText"
      ),
      stub_model(Story,
        :owner => nil,
        :points => 1,
        :status => "Status",
        :story_type => "Story Type",
        :requester => nil,
        :description => "MyText"
      )
    ])
  end

  it "renders a list of stories" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Status".to_s, :count => 2
    assert_select "tr>td", :text => "Story Type".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
