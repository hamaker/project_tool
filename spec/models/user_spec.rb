require 'spec_helper'

describe User do
  it { should have_many(:owned_stories) }
  it { should have_many(:requested_stories) }
end
