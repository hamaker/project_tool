require 'spec_helper'

describe Story do
  it { expect(subject).to belong_to(:owner).class_name(User)}
  it { expect(subject).to belong_to(:requester).class_name(User)}
  it { expect(subject).to have_many(:tasks) }
  it { expect(subject).to accept_nested_attributes_for(:tasks) }
  it 'responds to statuses' do
    expect(Story.statuses).to eq Story::STATUSES
  end
end
