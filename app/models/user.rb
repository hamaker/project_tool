class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :owned_stories, foreign_key: :owner_id, class_name: Story
  has_many :requested_stories, foreign_key: :requester_id, class_name: Story
end
