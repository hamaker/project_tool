class Story < ActiveRecord::Base
  STATUSES = {0 => 'unstarted', 1 => 'started', 2 => 'finished'}
  belongs_to :owner, class_name: User
  belongs_to :requester, class_name: User
  has_many :tasks

  accepts_nested_attributes_for :tasks
  def self.statuses
    STATUSES
  end
end
