json.array!(@tasks) do |task|
  json.extract! task, :id, :story_id, :name, :status
  json.url task_url(task, format: :json)
end
