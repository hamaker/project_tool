json.array!(@stories) do |story|
  json.extract! story, :id, :owner_id, :points, :status, :story_type, :requester_id, :description
  json.url story_url(story, format: :json)
end
